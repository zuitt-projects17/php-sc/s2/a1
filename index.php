<?php require_once "./code.php"; ?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S2 A1</title>
</head>
<body>


	<h1>Divisible of 5</h1>
	<?php divisibleBy5(); ?>



	<h1>Array Manipulation</h1>
	<?php array_push($students, 'John Smith'); ?>
	<pre><?php print_r($students); ?></pre>
	<p><?= count($students); ?></p>

	<?php array_push($students, 'Jane Smith'); ?>
	<pre><?php print_r($students); ?></pre>
	<p><?= count($students); ?></p>

	<?php array_shift($students); ?>
	<pre><?php print_r($students); ?></pre>
	<p><?= count($students); ?></p>


</body>
</html>